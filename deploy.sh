#!/bin/sh
set -e

make install

# get last build and check if it is more recent than cur

oc get builds   
oc start-build blogs
oc build-logs blogs-3
