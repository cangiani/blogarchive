# To do it in prod, switch to the good project and define TGPRG env variable: 
# (export TGPRG=inform-prod ; oc project $TGPRG && make ) or
# (export TGPRG=inform-prod ; oc project $TGPRG && make deploy)

SHELL=/bin/bash
-include .env

TGPRG ?= svc0041p-web-archiving
IMGVERSION ?= 1.0
IMAGE ?= quay-its.epfl.ch/svc0041/blogarchive:$(IMGVERSION)
OCPRG := $(shell oc project -q)
ERBS=$(wildcard ocsrc/*.yml)
YMLS=$(patsubst ocsrc/%,ocyml/%,$(ERBS))

export

all: $(YMLS)

clean:
	rm $(YMLS)

ocyml/%.yml: ocsrc/%.yml
	cat config.erb $< | erb -T 2 >$@

ocsrc/%.yml: .env config.erb
	touch $@

checkproj:
	@[ "$(TGPRG)" == "$(OCPRG)" ] || oc project $(TGPRG) || ( echo "Unexpected oc project: $(OCPRG)" ;  exit 1 )

install: checkproj $(YMLS)
	for f in ocyml/*.yml ; do oc apply -f $$f ; done

build:
	docker build -t $(IMAGE) apache-tequila

push: build
	docker login quay-its.epfl.ch
	docker push $(IMAGE)
	docker logout quay-its.epfl.ch

.PHONY: all checkproj install build push